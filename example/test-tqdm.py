from tqdm import tqdm
from time import sleep


def basic_loop(num_iterations: int, sleep_time: float = 0.01):
    """
    loop over something for a specified number of times ...

    ... and do `sleep()` (wait) a hundredth of a second each loop. simple!

    (note that `range(integer)` is equivalent to 
    """

    for i in tqdm(range(int(num_iterations))):

        sleep(sleep_time)

    print(f"did {num_iterations} loops, sleeping {str(sleep_time)} each time")


if __name__ == "__main__":
    # note the line above and all 'below' it are only invoked if we run the file itself ...
    # ... this is useful for testing, since we can specify generic code above this line
    # and specific (testing) code below it

    n = 100

    basic_loop(n)
