
# so you're interested in python virtual environments ...

> :warning: this isn't meant to be authoritative or exhaustive or correct, but merely a glimpse into how one developer chooses to manage python virtual environments - peruse at your own risk

## quick setup

> ! this isn't meant to work with a conda terminal; try powershell or terminal instead

* code editor; recommended: vscode
* python; if you don't have it, you can download it here: https://www.python.org/downloads/
    * note the version you download (e.g., 3.10.7 is latest as of time of writing)
* test that vscode works
    * open a terminal (terminal on mac, powershell on windows)
    * type `code .` and hit `enter`
    * vscode should open in your 'current' folder
* test that python works
    * in a terminal, `py --version` (or `python --version`, or maybe `python3 --version` - depends on how it was installed... can be weird or annoying)
    * whichever command above returns the version downloaded (or desired) is the 'py' command you'll use to invoke python
    * in my case, testing on windows: `py --version` returns what i want

```ps1
PS C:\Users\Alexander> py --version
Python 3.10.7
```

### a couple of small notes

* a default python installation includes a number of additional libraries and resources to help python function normally
* here, we're most interested in the default `venv` package, an easy option for out-of-the-box virtual environments for python
* check that `venv` is available in your install with `py -m venv --help` - you should see usage guidance if you have it, or an angry warning if it's not found
    * note the `-m` 'flag' just tells python (`py`) to invoke a specific package (`m`odule) - this isn't always necessary to use

```ps1
PS C:\Users\Alexander> py -m venv --help
usage: venv [-h] [--system-site-packages] [--symlinks | --copies] [--clear] [--upgrade] [--without-pip]
            [--prompt PROMPT] [--upgrade-deps]
            ENV_DIR [ENV_DIR ...]

Creates virtual Python environments in one or more target directories.

...
```

## virtual environment

why a virtual environment (hereafter, 'venv')? your code will run much more consistently if you can assert exactly which packages/modules it has access to, down to the specific version ... and this means anyone else running your code can (reasonably) expect the same results

### navigate to your project

if your project is encapsulated in a particular folder, navigate to it; if you cloned a project with git, then it's probably the highest-level folder for that project in particular. below, i create a new folder and navigate to it:

```ps1
PS C:\Users\Alexander> mkdir venv-demo


    Directory: C:\Users\Alexander


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----          9/6/2022   9:24 PM                venv-demo


PS C:\Users\Alexander> cd .\venv-demo\
PS C:\Users\Alexander\venv-demo>
```

### create a venv

this is easy: you just need to be explicit with the python version you're using. here, i verify again that `py` returns the version of python i want to use, then i create the virtual environment (called 'ag-venv0 - call it whatever you want); if i `ls` after creating it, i can see a new folder:

```ps1
PS C:\Users\Alexander\venv-demo> py --version
Python 3.10.7
PS C:\Users\Alexander\venv-demo> py -m venv ag-venv
PS C:\Users\Alexander\venv-demo> ls


    Directory: C:\Users\Alexander\venv-demo


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----          9/6/2022   9:27 PM                ag-venv


PS C:\Users\Alexander\venv-demo>
```

### use the venv

you've created the venv, great! activate it by just running the 'activate' script directly in the terminal - note the terminal now shows the name of your venv as a 'prefix' to the command line. 

once the venv is 'active', you can invoke your chosen python version with the basic `python` command (tested below with the 'version' flag); note that before the venv is active, the version invoked by `python` may not be the one you created the venv with:

```ps1
PS C:\Users\Alexander\venv-demo> python --version
Python 3.7.1
PS C:\Users\Alexander\venv-demo> .\ag-venv\Scripts\activate
(ag-venv) PS C:\Users\Alexander\venv-demo> python --version
Python 3.10.7
```

> on a mac, you would just run `source ag-venv/bin/activate` to 'activate' the venv - otherwise this entire process is similar enough

#### error i encountered

i saw an error preventing me from running the above script ('running scripts is disabled on this system'), so i followed steps in the link below to resolve the problem before 'activating' as above:

https://www.stanleyulili.com/powershell/solution-to-running-scripts-is-disabled-on-this-system-error-on-powershell/

### stop using the venv

your venv will remain 'active' if you leave your working directory in terminal, which may not be what you want - or perhaps you activated the wrong one (yes, you can have multiple! just name them differently). here, i use the `deactivate` command when i leave my project and realize the venv is still active:

```ps1
(ag-venv) PS C:\Users\Alexander\venv-demo> cd ..
(ag-venv) PS C:\Users\Alexander> deactivate
PS C:\Users\Alexander>
```

> `deactivate` is only available while a venv is 'active'

### erase the venv

it's just a folder; you can delete the folder (`ag-venv` for me itself using your preferred method, though i would recommend deactivating the venv first (see above)

## installing things in your venv

you have a venv, great! once it's active, let's use it -

### testing `pip install`

here, i pick one of my favorite packages (`tqdm`) and install it - it's also very small and makes for an easy demo - note that i check the `pip` version first:

```ps1
(ag-venv) PS C:\Users\Alexander\venv-demo> pip --version
pip 22.2.2 from C:\Users\Alexander\venv-demo\ag-venv\lib\site-packages\pip (python 3.10)
(ag-venv) PS C:\Users\Alexander\venv-demo> pip install tqdm
Collecting tqdm
  Downloading tqdm-4.64.1-py2.py3-none-any.whl (78 kB)
     ---------------------------------------- 78.5/78.5 kB 4.5 MB/s eta 0:00:00
Collecting colorama
  Downloading colorama-0.4.5-py2.py3-none-any.whl (16 kB)
Installing collected packages: colorama, tqdm
Successfully installed colorama-0.4.5 tqdm-4.64.1
(ag-venv) PS C:\Users\Alexander\venv-demo>
```

> `pip` is the built-in command for managing python package installation

the install was successful. yay! next, i remove it:

```ps1
(ag-venv) PS C:\Users\Alexander\venv-demo> pip uninstall tqdm
Found existing installation: tqdm 4.64.1
Uninstalling tqdm-4.64.1:
  Would remove:
    c:\users\alexander\venv-demo\ag-venv\lib\site-packages\tqdm-4.64.1.dist-info\*
    c:\users\alexander\venv-demo\ag-venv\lib\site-packages\tqdm\*
    c:\users\alexander\venv-demo\ag-venv\scripts\tqdm.exe
Proceed (Y/n)? Y
  Successfully uninstalled tqdm-4.64.1
(ag-venv) PS C:\Users\Alexander\venv-demo>
```

### installing from a file

it's common in python projects to use a 'requirements' file to track which packages you use; that way, someone else (future you?) knows what needs to be installed. i'm going to create a basic `requirements.txt` file and add my package, `tqdm`, as the only item in it. then, i install `tqdm` from that file using the `-r` flag for `pip`:

```ps1
(ag-venv) PS C:\Users\Alexander\venv-demo> echo tqdm > requirements.txt
(ag-venv) PS C:\Users\Alexander\venv-demo> cat .\requirements.txt
tqdm
(ag-venv) PS C:\Users\Alexander\venv-demo> pip install -r .\requirements.txt
Collecting tqdm
  Using cached tqdm-4.64.1-py2.py3-none-any.whl (78 kB)
Requirement already satisfied: colorama in c:\users\alexander\venv-demo\ag-venv\lib\site-packages (from tqdm->-r .\requirements.txt (line 1)) (0.4.5)
Installing collected packages: tqdm
Successfully installed tqdm-4.64.1
(ag-venv) PS C:\Users\Alexander\venv-demo>
```

> note that requirements files are particularly helpful because you can commit them with git and share them as part of the project!

### wait, which version did i install?

maybe you need to check which version of package(s) is installed; just use `pip freeze`:

```ps1
(ag-venv) PS C:\Users\Alexander\venv-demo> pip freeze
colorama==0.4.5
tqdm==4.64.1
(ag-venv) PS C:\Users\Alexander\venv-demo>
```

> i did not install `colorama` specifically - this is a __dependency__ of `tqdm` that was installed automatically

### coming back to a project after a while

if you use a lot of packages, or if you use popular packages (like `pandas` or `numpy`), then updates for your packages might be available after a while. here, you can use the `--upgrade` flag for `pip install`. since i already have the latest version, nothing happens if i update from my file or `tqdm` specifically:

```ps1
(ag-venv) PS C:\Users\Alexander\venv-demo> pip install --upgrade tqdm
Requirement already satisfied: tqdm in c:\users\alexander\venv-demo\ag-venv\lib\site-packages (4.64.1)
Requirement already satisfied: colorama in c:\users\alexander\venv-demo\ag-venv\lib\site-packages (from tqdm) (0.4.5)
(ag-venv) PS C:\Users\Alexander\venv-demo> pip install --upgrade -r .\requirements.txt
Requirement already satisfied: tqdm in c:\users\alexander\venv-demo\ag-venv\lib\site-packages (from -r .\requirements.txt (line 1)) (4.64.1)
Requirement already satisfied: colorama in c:\users\alexander\venv-demo\ag-venv\lib\site-packages (from tqdm->-r .\requirements.txt (line 1)) (0.4.5)
(ag-venv) PS C:\Users\Alexander\venv-demo>
```

### getting clever with requirements files

there's nothing stopping us from having multiple requirements files; this is actually pretty helpful in certain cases. i prefer to use a `requirements.in` (`.in` is nonsensical, it's still just a text file) to hold 'unversioned' packages i specify, and a `requirements.txt` to hold the specific versions of ALL packages, including dependencies, so anyone can reproduce exactly* the python setup i have.

```ps1
(ag-venv) PS C:\Users\Alexander\venv-demo> echo tqdm > .\requirements.in
(ag-venv) PS C:\Users\Alexander\venv-demo> cat .\requirements.in
tqdm
(ag-venv) PS C:\Users\Alexander\venv-demo> pip install -r .\requirements.in
Successfully installed tqdm-4.64.1
(ag-venv) PS C:\Users\Alexander\venv-demo> pip freeze *> requirements.txt
(ag-venv) PS C:\Users\Alexander\venv-demo> cat .\requirements.txt
colorama==0.4.5
tqdm==4.64.1
(ag-venv) PS C:\Users\Alexander\venv-demo>
```

> *there will always be differences outside of python packages that might impact how things run - for example, dependencies on other programming languages like C (common issue for `numpy`) or Java

## use python in the terminal with your venv

just run `python`:

```ps1
(ag-venv) PS C:\Users\Alexander\venv-demo> python
Python 3.10.7 (tags/v3.10.7:6cc6b13, Sep  5 2022, 14:08:36) [MSC v.1933 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> print("hello world")
hello world
>>>
```

## put it all together

there's an example folder in this project - feel free to copy it to get a feel for what's happening! try the following:

1. create your virtual environment and activate it
2. install requirements using `requirements.in`
3. run the python file (using `python test-tqdm.py`)
4. try using python directly in your venv in the terminal with `python`
